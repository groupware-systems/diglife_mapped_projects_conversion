#!/bin/sh
# Copyright (C) 2019-2020 Stephan Kreutzer
#
# This file is part of diglife_mapped_projects_conversion.
#
# diglife_mapped_projects_conversion is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3 or any later
# version of the license, as published by the Free Software Foundation.
#
# diglife_mapped_projects_conversion is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License 3 for more details.
#
# You should have received a copy of the GNU Affero General Public License 3
# along with diglife_mapped_projects_conversion. If not, see <http://www.gnu.org/licenses/>.

java -cp ./digital_publishing_workflow_tools/json_to_xml/json_to_xml_2/ json_to_xml_2 ./jobfile_json_to_xml_2.xml resultinfo_json_to_xml_2.xml
java -cp ./digital_publishing_workflow_tools/xml_xslt_transformator/xml_xslt_transformator_1/ xml_xslt_transformator_1 ./jobfile_xml_xslt_transformator_1_preparations.xml resultinfo_xml_xslt_transformator_1_preparations.xml

cat schemata.txt | sort | uniq > schemata_unique.txt
rm schemata.txt

cat names_1530468464654.txt | sort | uniq > names_unique_1530468464654.txt
rm names_1530468464654.txt
cat names_1530787479491.txt | sort | uniq > names_unique_1530787479491.txt
rm names_1530787479491.txt
cat names_1531157324751.txt | sort | uniq > names_unique_1531157324751.txt
rm names_1531157324751.txt

java -cp ./diglife_mapped_projects_preparer_1/ diglife_mapped_projects_preparer_1 ./output_temp.xml ./output.xml
rm ./output_temp.xml

java -cp ./digital_publishing_workflow_tools/xml_xslt_transformator/xml_xslt_transformator_1/ xml_xslt_transformator_1 ./jobfile_xml_xslt_transformator_1_results.xml resultinfo_xml_xslt_transformator_1_results.xml

java -cp ./automated_digital_publishing/xml_split/xml_split1/ xml_split1 ./output.xml ./automated_digital_publishing/xml_split/xml_split1/entities/config.xml ./jobfile_xml_split1_1.xml ./output/
java -cp ./automated_digital_publishing/xml_split/xml_split1/ xml_split1 ./murmurations.xml ./automated_digital_publishing/xml_split/xml_split1/entities/config.xml ./jobfile_xml_split1_2.xml ./murmurations/

rm ./output/info.xml
rm ./murmurations/info.xml
