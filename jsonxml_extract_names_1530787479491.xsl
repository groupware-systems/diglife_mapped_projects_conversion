<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright (C) 2019 Stephan Kreutzer

This file is part of diglife_mapped_projects_conversion.

diglife_mapped_projects_conversion is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License version 3 or any later
version of the license, as published by the Free Software Foundation.

diglife_mapped_projects_conversion is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License 3 for more details.

You should have received a copy of the GNU Affero General Public License 3
along with diglife_mapped_projects_conversion. If not, see <http://www.gnu.org/licenses/>.
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="text" omit-xml-declaration="yes" indent="no"/>

  <xsl:template match="/">
    <xsl:apply-templates select=".//array[@name='elements']"/>
  </xsl:template>

  <xsl:template match="//array[@name='elements']">
    <xsl:apply-templates select="./object"/>
  </xsl:template>

  <xsl:template match="//array[@name='elements']/object">
    <xsl:if test="./literal[@name='schema']/text() = '1530787479491'">
      <xsl:apply-templates/>
    </xsl:if>
  </xsl:template>

  <xsl:template match="//array[@name='elements']/object//object[@name]">
    <xsl:value-of select="@name"/>
    <xsl:text>&#xA;</xsl:text>
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="//array[@name='elements']/object//array[@name]">
    <xsl:value-of select="@name"/>
    <xsl:text>&#xA;</xsl:text>
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="//array[@name='elements']/object//string[@name]">
    <xsl:value-of select="@name"/>
    <xsl:text>&#xA;</xsl:text>
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="//array[@name='elements']/object//literal[@name]">
    <xsl:value-of select="@name"/>
    <xsl:text>&#xA;</xsl:text>
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="text()|@*"/>

</xsl:stylesheet>
