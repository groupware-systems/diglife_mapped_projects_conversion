/* Copyright (C) 2014-2019 Stephan Kreutzer
 *
 * This file is part of diglife_mapped_projects_preparer_1.
 *
 * diglife_mapped_projects_preparer_1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * diglife_mapped_projects_preparer_1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with diglife_mapped_projects_preparer_1. If not, see <http://www.gnu.org/licenses/>.
 */


import java.io.File;
import java.net.URLDecoder;
import java.io.UnsupportedEncodingException;
import java.io.IOException;
import javax.xml.stream.XMLInputFactory;
import java.io.InputStream;
import java.io.FileInputStream;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.events.XMLEvent;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.FileOutputStream;
import javax.xml.namespace.QName;
import java.util.Iterator;
import javax.xml.stream.events.Namespace;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.events.Comment;
import java.io.FileNotFoundException;
import javax.xml.stream.XMLStreamException;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.TimeZone;



public class diglife_mapped_projects_preparer_1
{
    public static void main(String args[])
    {
        System.out.print("diglife_mapped_projects_preparer_1 Copyright (C) 2014-2019 Stephan Kreutzer\n" +
                         "This program comes with ABSOLUTELY NO WARRANTY.\n" +
                         "This is free software, and you are welcome to redistribute it\n" +
                         "under certain conditions. See the GNU Affero General Public License 3\n" +
                         "or any later version for details.\n\n");

        if (args.length < 2)
        {
            System.out.println("Usage:\n\n\tdiglife_mapped_projects_preparer_1 input-file output-file\n");
            System.exit(1);
        }

        String programPath = diglife_mapped_projects_preparer_1.class.getProtectionDomain().getCodeSource().getLocation().getPath();

        try
        {
            programPath = new File(programPath).getCanonicalPath() + File.separator;
            programPath = URLDecoder.decode(programPath, "UTF-8");
        }
        catch (UnsupportedEncodingException ex)
        {
            System.out.println("diglife_mapped_projects_preparer_1: Can't get canonical program path.");
            System.exit(1);
        }
        catch (IOException ex)
        {
            System.out.println("diglife_mapped_projects_preparer_1: Can't get canonical program path.");
            System.exit(1);
        }

        File inFile = new File(args[0]);

        if (inFile.exists() != true)
        {
            System.out.println("diglife_mapped_projects_preparer_1: '" + inFile.getAbsolutePath() + "' doesn't exist.");
            System.exit(1);
        }

        if (inFile.isFile() != true)
        {
            System.out.println("diglife_mapped_projects_preparer_1: '" + inFile.getAbsolutePath() + "' isn't a file.");
            System.exit(1);
        }

        if (inFile.canRead() != true)
        {
            System.out.println("diglife_mapped_projects_preparer_1: '" + inFile.getAbsolutePath() + "' isn't readable.");
            System.exit(1);
        }

        File outFile = new File(args[1]);

        try
        {
            if (outFile.getCanonicalPath().equals(inFile.getCanonicalPath()) == true)
            {
                System.out.println("diglife_mapped_projects_preparer_1: Input and output file are the same, can't read and write to '" + inFile.getAbsolutePath() + "' at the same time.");
                System.exit(1);
            }
        }
        catch (IOException ex)
        {
            System.out.println("diglife_mapped_projects_preparer_1: Can't get canonical path of either the input file or the output file.");
            System.exit(1);
        }

        diglife_mapped_projects_preparer_1 instance = new diglife_mapped_projects_preparer_1();

        instance.convert(inFile, outFile);
    }

    public int convert(File inFile, File outFile)
    {
        try
        {
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            InputStream in = new FileInputStream(inFile);
            this.eventReader = inputFactory.createXMLEventReader(in, "UTF8");

            XMLEvent event = null;

            this.writer = new BufferedWriter(
                          new OutputStreamWriter(
                          new FileOutputStream(outFile),
                          "UTF8"));

            this.writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
            this.writer.write("<!-- This file was generated by diglife_mapped_projects_preparer_1 of diglife_mapped_projects_conversion, which is free software licensed under the GNU Affero General Public License 3 or any later version. -->\n");

            while (this.eventReader.hasNext() == true)
            {
                event = this.eventReader.nextEvent();

                if (event.isStartElement() == true)
                {
                    QName elementName = event.asStartElement().getName();
                    String fullElementName = elementName.getLocalPart();

                    if (elementName.getPrefix().isEmpty() != true)
                    {
                        fullElementName = elementName.getPrefix() + ":" + fullElementName;
                    }

                    if (fullElementName.equals("last-modified") == true)
                    {
                        HandleElementLastModified(fullElementName);
                    }
                    else
                    {
                        HandleElement(fullElementName, event);
                    }
                }
                else if (event.isEndElement() == true)
                {
                    QName elementName = event.asEndElement().getName();
                    String fullElementName = elementName.getLocalPart();

                    if (elementName.getPrefix().isEmpty() != true)
                    {
                        fullElementName = elementName.getPrefix() + ":" + fullElementName;
                    }

                    this.writer.write("</" + fullElementName + ">");
                }
                else if (event.isCharacters() == true)
                {
                    event.writeAsEncodedUnicode(this.writer); 
                }
                else if (event.getEventType() == XMLStreamConstants.COMMENT)
                {
                    this.writer.write("<!--" + ((Comment)event).getText() + "-->");
                }
            }

            this.writer.flush();
            this.writer.close();
        }
        catch (FileNotFoundException ex)
        {
            System.out.println("diglife_mapped_projects_preparer_1: An error occurred while conversion.");
            System.exit(1);
        }
        catch (XMLStreamException ex)
        {
            System.out.println("diglife_mapped_projects_preparer_1: An error occurred while conversion.");
            System.exit(1);
        }
        catch (IOException ex)
        {
            System.out.println("diglife_mapped_projects_preparer_1: An error occurred while conversion.");
            System.exit(1);
        }

        return 0;
    }

    public int HandleElementLastModified(String fullElementName) throws IOException, XMLStreamException
    {
        this.writer.write("<last-modified>");

        if (this.eventReader.hasNext() != true)
        {
            System.out.println("diglife_mapped_projects_preparer_1: An error occurred while converting '" + fullElementName + "'.");
            System.exit(1);
        }

        XMLEvent event = this.eventReader.nextEvent();

        if (event.isCharacters() != true)
        {
            System.out.println("diglife_mapped_projects_preparer_1: An error occurred while converting '" + fullElementName + "'.");
            System.exit(1);
        }

        long timestamp = Long.parseLong(event.asCharacters().getData());
        Date lastModified = new Date(timestamp);

        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        dateFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        this.writer.write(dateFormatter.format(lastModified));

        if (this.eventReader.hasNext() != true)
        {
            System.out.println("diglife_mapped_projects_preparer_1: An error occurred while converting '" + fullElementName + "'.");
            System.exit(1);
        }

        event = this.eventReader.nextEvent();

        if (event.isEndElement() != true)
        {
            System.out.println("diglife_mapped_projects_preparer_1: An error occurred while converting '" + fullElementName + "'.");
            System.exit(1);
        }

        QName elementName = event.asEndElement().getName();
        String fullElementNameEnd = elementName.getLocalPart();

        if (elementName.getPrefix().isEmpty() != true)
        {
            fullElementNameEnd = elementName.getPrefix() + ":" + fullElementNameEnd;
        }

        if (fullElementNameEnd.equals(fullElementName) != true)
        {
            System.out.println("diglife_mapped_projects_preparer_1: An error occurred while converting '" + fullElementName + "'.");
            System.exit(1);
        }

        this.writer.write("</" + fullElementName + ">");

        return 0;
    }

    public int HandleElement(String fullElementName, XMLEvent event) throws IOException
    {
        this.writer.write("<" + fullElementName);

        // http://coding.derkeiler.com/Archive/Java/comp.lang.java.help/2008-12/msg00090.html
        @SuppressWarnings("unchecked")
        Iterator<Namespace> namespaces = (Iterator<Namespace>)event.asStartElement().getNamespaces();

        if (namespaces.hasNext() == true)
        {
            Namespace namespace = namespaces.next();

            if (namespace.isDefaultNamespaceDeclaration() == true &&
                namespace.getPrefix().length() <= 0)
            {
                this.writer.write(" xmlns=\"" + namespace.getNamespaceURI() + "\"");
            }
            else
            {
                this.writer.write(" xmlns:" + namespace.getPrefix() + "=\"" + namespace.getNamespaceURI() + "\"");
            }
        }

        // http://coding.derkeiler.com/Archive/Java/comp.lang.java.help/2008-12/msg00090.html
        @SuppressWarnings("unchecked")
        Iterator<Attribute> attributes = (Iterator<Attribute>)event.asStartElement().getAttributes();

        while (attributes.hasNext() == true)
        {  
            Attribute attribute = attributes.next();
            QName attributeName = attribute.getName();
            String fullAttributeName = attributeName.getLocalPart();

            if (attributeName.getPrefix().length() > 0)
            {
                fullAttributeName = attributeName.getPrefix() + ":" + fullAttributeName;
            }

            String attributeValue = attribute.getValue();

            // Ampersand needs to be the first, otherwise it would double-encode
            // other entities.
            attributeValue = attributeValue.replaceAll("&", "&amp;");
            attributeValue = attributeValue.replaceAll("\"", "&quot;");
            attributeValue = attributeValue.replaceAll("'", "&apos;");
            attributeValue = attributeValue.replaceAll("<", "&lt;");
            attributeValue = attributeValue.replaceAll(">", "&gt;");

            this.writer.write(" " + fullAttributeName + "=\"" + attributeValue + "\"");
        }

        this.writer.write(">");

        return 0;
    }

    protected BufferedWriter writer;
    protected XMLEventReader eventReader;
}
