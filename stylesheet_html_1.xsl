<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright (C) 2020 Stephan Kreutzer

This file is part of diglife_mapped_projects_conversion.

diglife_mapped_projects_conversion is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License version 3 or any later
version of the license, as published by the Free Software Foundation.

diglife_mapped_projects_conversion is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License 3 for more details.

You should have received a copy of the GNU Affero General Public License 3
along with diglife_mapped_projects_conversion. If not, see <http://www.gnu.org/licenses/>.
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml" xmlns:src="htx-scheme-id://org.groupware-systems.20190227T133048Z/clients/com.diglife.20161001T183139Z/projects-diglife.20191015T223948Z" exclude-result-prefixes="src">
  <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="no" doctype-public="-//W3C//DTD XHTML 1.1//EN" doctype-system="http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"/>

  <xsl:template match="/">
    <html version="-//W3C//DTD XHTML 1.1//EN" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.w3.org/1999/xhtml http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd" xml:lang="en" lang="en">
      <head>
        <meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8"/>
        <xsl:text>&#x0A;</xsl:text>
        <xsl:comment> This file was created by stylesheet_html_1.xsl of diglife_mapped_projects_conversion, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://groupware-systems.org). </xsl:comment>
        <xsl:text>&#x0A;</xsl:text>
        <title>DigLife’s Directory of Decentralization Projects</title>
        <style type="text/css">
          body
          {
              font-family: sans-serif;
          }

          .table
          {
              display: table;
          }

          .tr
          {
              display: table-row;
          }

          .thead
          {
              display: table-header-group;
          }

          .tbody
          {
              display: table-row-group;
          }

          .tfoot
          {
              display: table-footer-group;
          }

          .th
          {
              display: table-cell;
              font-weight: bold;
              padding: 5px;
          }

          .td
          {
              display: table-cell;
              padding: 5px;
          }
        </style>
      </head>
      <body>
        <div>
          <h1>DigLife’s Directory of Decentralization Projects</h1>
          <div>
            <xsl:apply-templates select="./src:projects/src:project"/>
          </div>
          <div>
            <p>
              Copyright (C) 2017-2020 Digital Life Collective, published under the <a href="https://github.com/DigitalLifeCollective/mapped-projects/blob/development/LICENSE">X11 license</a>. Original source: <a href="https://github.com/DigitalLifeCollective/mapped-projects">https://github.com/DigitalLifeCollective/mapped-projects</a>. Converter: <a href="https://gitlab.com/groupware-systems/diglife_mapped_projects_conversion">https://gitlab.com/groupware-systems/diglife_mapped_projects_conversion</a>.
            </p>
          </div>
        </div>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="/src:projects/src:project">
    <div>
      <xsl:apply-templates select="./src:common-name | ./src:label"/>
      <div>
        <!--xsl:apply-templates select="./src:image-url"/-->
        <div class="table">
          <xsl:apply-templates select="./src:id"/>
          <xsl:apply-templates select="./src:tag-line"/>
          <xsl:apply-templates select="./src:description-1"/>
          <xsl:apply-templates select="./src:description-2"/>
          <xsl:apply-templates select="./src:maturity"/>
          <xsl:apply-templates select="./src:download-required"/>
          <xsl:apply-templates select="./src:encryption"/>
          <xsl:apply-templates select="./src:url"/>
          <xsl:apply-templates select="./src:participation-url"/>
          <!-- Also: video-src for embedded! -->
          <xsl:apply-templates select="./src:video-url"/>
          <xsl:apply-templates select="./src:github-profile"/>
          <xsl:apply-templates select="./src:github-repository"/>
          <xsl:apply-templates select="./src:twitter-profile"/>
          <xsl:apply-templates select="./src:open-feedback"/>
          <xsl:apply-templates select="./src:last-modified"/>
        </div>
        <xsl:apply-templates select="./src:software-licenses"/>
        <xsl:apply-templates select="./src:areas-of-work"/>
        <xsl:apply-templates select="./src:project-types"/>
        <xsl:apply-templates select="./src:groups"/>
        <xsl:apply-templates select="./src:stack"/>
        <xsl:apply-templates select="./src:network-topologies"/>
        <xsl:apply-templates select="./src:regional-tractions"/>
        <xsl:apply-templates select="./src:suggested-areas-of-work"/>
        <xsl:apply-templates select="./src:suggested-values"/>
      </div>
    </div>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:common-name">
    <h2>
      <xsl:apply-templates/>
    </h2>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:common-name//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:label">
    <xsl:if test="not(./../src:common-name)">
      <h2>
        <xsl:apply-templates/>
      </h2>
    </xsl:if>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:label//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:id">
    <div class="tr">
      <span class="td">
        <xsl:text>ID</xsl:text>
      </span>
      <span class="td">
        <xsl:apply-templates/>
      </span>
    </div>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:id//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:tag-line">
    <div class="tr">
      <span class="td">
        <xsl:text>Tagline:</xsl:text>
      </span>
      <span class="td">
        <xsl:apply-templates/>
      </span>
    </div>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:tag-line//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:description-1">
    <div class="tr">
      <span class="td">
        <xsl:text>Description 1:</xsl:text>
      </span>
      <span class="td">
        <xsl:apply-templates/>
      </span>
    </div>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:description-1//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:description-2">
    <div class="tr">
      <span class="td">
        <xsl:text>Description 2:</xsl:text>
      </span>
      <span class="td">
        <xsl:apply-templates/>
      </span>
    </div>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:description-2//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:maturity">
    <div class="tr">
      <span class="td">
        <xsl:text>Maturity:</xsl:text>
      </span>
      <span class="td">
        <xsl:apply-templates/>
      </span>
    </div>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:maturity//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:download-required">
    <div class="tr">
      <span class="td">
        <xsl:text>Download required:</xsl:text>
      </span>
      <span class="td">
        <xsl:apply-templates/>
      </span>
    </div>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:download-required//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:encryption">
    <div class="tr">
      <span class="td">
        <xsl:text>Encryption:</xsl:text>
      </span>
      <span class="td">
        <xsl:apply-templates/>
      </span>
    </div>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:encryption//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:open-feedback">
    <div class="tr">
      <span class="td">
        <xsl:text>Open feedback:</xsl:text>
      </span>
      <span class="td">
        <xsl:apply-templates/>
      </span>
    </div>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:open-feedback//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:last-modified">
    <div class="tr">
      <span class="td">
        <xsl:text>Last modified:</xsl:text>
      </span>
      <span class="td">
        <xsl:apply-templates/>
      </span>
    </div>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:last-modified//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:url">
    <div class="tr">
      <span class="td"/>
      <span class="td">
        <a>
          <xsl:attribute name="href">
            <xsl:apply-templates/>
          </xsl:attribute>
          <xsl:text>Link</xsl:text>
        </a>
      </span>
    </div>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:url//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:participation-url">
    <div class="tr">
      <span class="td"/>
      <span class="td">
        <a>
          <xsl:attribute name="href">
            <xsl:apply-templates/>
          </xsl:attribute>
          <xsl:text>Participation link</xsl:text>
        </a>
      </span>
    </div>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:participation-url//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:video-url">
    <div class="tr">
      <span class="td"/>
      <span class="td">
        <a>
          <xsl:attribute name="href">
            <xsl:apply-templates/>
          </xsl:attribute>
          <xsl:text>Video</xsl:text>
        </a>
      </span>
    </div>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:video-url//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:github-profile">
    <div class="tr">
      <span class="td">
        <xsl:text>GitHub profile:</xsl:text>
      </span>
      <span class="td">
        <xsl:apply-templates/>
      </span>
    </div>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:github-profile//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:github-repository">
    <div class="tr">
      <span class="td">
        <xsl:text>GitHub repository:</xsl:text>
      </span>
      <span class="td">
        <xsl:apply-templates/>
      </span>
    </div>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:github-repository//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:twitter-profile">
    <div class="tr">
      <span class="td">
        <xsl:text>Twitter:</xsl:text>
      </span>
      <span class="td">
        <xsl:apply-templates/>
      </span>
    </div>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:twitter-profile//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:software-licenses">
    <div>
      <p>
        Software licenses:
      </p>
      <ul>
        <xsl:apply-templates/>
      </ul>
    </div>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:software-licenses/src:software-license">
    <li>
      <xsl:apply-templates/>
    </li>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:software-licenses/src:software-license//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:areas-of-work">
    <div>
      <p>
        Area of work:
      </p>
      <ul>
        <xsl:apply-templates/>
      </ul>
    </div>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:areas-of-work/src:area-of-work">
    <li>
      <xsl:apply-templates/>
    </li>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:areas-of-work/src:area-of-work//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:project-types">
    <div>
      <p>
        Project type:
      </p>
      <ul>
        <xsl:apply-templates/>
      </ul>
    </div>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:project-types/src:project-type">
    <li>
      <xsl:apply-templates/>
    </li>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:project-types/src:project-type//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:groups">
    <div>
      <p>
        Groups:
      </p>
      <ul>
        <xsl:apply-templates/>
      </ul>
    </div>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:groups/src:group">
    <li>
      <xsl:apply-templates/>
    </li>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:groups/src:group//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:image-url">
    <img style="width: 200px;">
      <xsl:attribute name="src">
        <xsl:apply-templates/>
      </xsl:attribute>
    </img>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:image-url//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:stack">
    <div>
      <p>
        Stack:
      </p>
      <ul>
        <xsl:apply-templates/>
      </ul>
    </div>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:stack/src:stack-component">
    <li>
      <xsl:apply-templates/>
    </li>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:stack/src:stack-component//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:network-topologies">
    <div>
      <p>
        Network topologies:
      </p>
      <ul>
        <xsl:apply-templates/>
      </ul>
    </div>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:network-topologies/src:network-topology">
    <li>
      <xsl:apply-templates/>
    </li>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:network-topologies/src:network-topology//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:regional-tractions">
    <div>
      <p>
        Regional tractions:
      </p>
      <ul>
        <xsl:apply-templates/>
      </ul>
    </div>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:regional-tractions/src:regional-traction">
    <li>
      <xsl:apply-templates/>
    </li>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:regional-tractions/src:regional-traction//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:suggested-areas-of-work">
    <div>
      <p>
        Suggested areas of work:
      </p>
      <ul>
        <xsl:apply-templates/>
      </ul>
    </div>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:suggested-areas-of-work/src:suggested-area-of-work">
    <li>
      <xsl:apply-templates/>
    </li>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:suggested-areas-of-work/src:suggested-area-of-work//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:suggested-values">
    <div>
      <p>
        Suggested values:
      </p>
      <ul>
        <xsl:apply-templates/>
      </ul>
    </div>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:suggested-values/src:suggested-value">
    <li>
      <xsl:apply-templates/>
    </li>
  </xsl:template>

  <xsl:template match="/src:projects/src:project/src:suggested-values/src:suggested-value//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <!-- No <location/>, no <nodeTypes/>. -->

  <!-- No <feed/>. -->

  <xsl:template match="text()|node()|@*"/>

</xsl:stylesheet>
