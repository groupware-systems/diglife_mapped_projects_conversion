#!/bin/sh
# Copyright (C) 2019-2020 Stephan Kreutzer
#
# This file is part of diglife_mapped_projects_conversion.
#
# diglife_mapped_projects_conversion is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3 or any later
# version of the license, as published by the Free Software Foundation.
#
# diglife_mapped_projects_conversion is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License 3 for more details.
#
# You should have received a copy of the GNU Affero General Public License 3
# along with diglife_mapped_projects_conversion. If not, see <http://www.gnu.org/licenses/>.

sudo apt-get install wget unzip make default-jdk

wget https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/-/archive/master/digital_publishing_workflow_tools-master.zip
sleep 3s; wget https://gitlab.com/publishing-systems/automated_digital_publishing/-/archive/master/automated_digital_publishing-master.zip
sleep 3s; wget https://github.com/DigitalLifeCollective/mapped-projects/archive/development.zip
mv development.zip mapped-projects-development.zip

unzip ./mapped-projects-development.zip
mv ./mapped-projects-development ./mapped-projects

unzip ./digital_publishing_workflow_tools-master.zip
mv ./digital_publishing_workflow_tools-master/ ./digital_publishing_workflow_tools/
cd ./digital_publishing_workflow_tools/
make
java -cp ./workflows/setup/setup_1/ setup_1
cd ..

unzip ./automated_digital_publishing-master.zip
mv ./automated_digital_publishing-master/ ./automated_digital_publishing/
cd ./automated_digital_publishing/
make
java -cp ./workflows/ setup1
cd ..

make
